<?php
require 'checkdate.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" crossorigin="anonymous">
	<!-- jQuery library -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Date Check</title>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script>
	jQuery( document ).ready(function() {
	jQuery( "#start_date" ).datepicker();
	jQuery( "#end_date" ).datepicker();
	 
    jQuery('#dateform').submit(function(e) {
        e.preventDefault();
        jQuery.ajax({
            type: "POST",
            url: 'checkdate.php',
            data: jQuery(this).serialize(),
            success: function(response)
            {
				jQuery("#numberofdays").empty();
				jQuery("#workingdays").empty();
                jQuery( "#numberofdays" ).append( response);
            }
       });
     });
	});
	</script>
	
  </head>
  <body>
  <div class="container">
   <form method="post" name="dateform" id="dateform">
    <h2>Date</h2>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="first">Start Date</label>
          <input type="text" class="form-control" placeholder="Enter Start Date" name="start_date" id="start_date">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">
        <div class="form-group">
          <label for="last">End Date</label>
          <input type="text" class="form-control" placeholder="Enter End date" name="end_date" id="end_date">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>
	  <span class="row" id="numberofdays"></span>
	  <span class="row" id="workingdays"></span>
	  <div class="row"></div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</section>
 </body>
</html>

