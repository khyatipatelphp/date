<?php
 
class DateClass {

public function cal_days_dates()
{
	if(!empty($_POST['start_date']) && !empty($_POST['end_date']))
	{
		$start_date=$_POST['start_date'];
		$end_date=$_POST['end_date'];
		$datediff = strtotime($end_date)- strtotime($start_date) ;
		 
		echo "<br/>Number of days between two dates $start_date and $end_date is: " . round($datediff / (60 * 60 * 24)) . " day(s).";

	}	

}

public function workingdays()
{
	if(!empty($_POST['start_date']) && !empty($_POST['end_date']))
	{
	$start_date=$_POST['start_date'];
	$end_date=$_POST['end_date'];
	$workingDays = 0;
	 
	$startTimestamp = strtotime($start_date);
	$endTimestamp = strtotime($end_date);
	 
	for($i=$startTimestamp; $i<=$endTimestamp; $i = $i+(60*60*24) ){
	if(date("N",$i) <= 5) $workingDays = $workingDays + 1;
	}
	echo "<br/>Total number of working days between $start_date and $end_date is: " . $workingDays . " day(s).";
	}
	
}

function numWeeks(){
	if(!empty($_POST['start_date']) && !empty($_POST['end_date']))
	{
	$start_date=$_POST['start_date'];
	$end_date=$_POST['end_date'];
	
    //Create a DateTime object for the first date.
    $firstDate = new DateTime($start_date);
    //Create a DateTime object for the second date.
    $secondDate = new DateTime($end_date);
    //Get the difference between the two dates in days.
    $differenceInDays = $firstDate->diff($secondDate)->days;
    //Divide the days by 7
    $differenceInWeeks = $differenceInDays / 7;
    //Round down with floor and return the difference in weeks.
    echo "<br/>Number of complete weeks between $start_date and $end_date is: " . floor($differenceInWeeks) . " week(s).";

	}
}


}
$DateClass = new DateClass();
 $DateClass->cal_days_dates();
  $DateClass->workingdays();
 $DateClass->numWeeks();
 
?>